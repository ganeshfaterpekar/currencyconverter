//
//  CucumberishInitializer.swift
//  CurrencyConverterUITests
//
//  Created by Ganesh Faterpekar on 6/10/20.
//

import Foundation
import Cucumberish

class CucumberishInitializer: NSObject {
    
    @objc class func setupCucumberish() {
        
        before({ _ in
            CurrencyConverterSetup().run()
            CurrencyConverterCalculation().run()
        })
        
        let bundle = Bundle(for: CucumberishInitializer.self)
        Cucumberish.executeFeatures(inDirectory: "Features", from: bundle, includeTags: nil, excludeTags: nil)
        
    }
    
}
