
Feature: Basic UI Setup

Scenario: As a user, I want verify the Basic UI is correct
Given I launch the app
Then I should see Currency Calculator


Scenario: As a user, I want to convert currency From USD to AUD
Given I launch the app
When I Enter "5" in the from currency field
Then I should see correct results in to currency


Scenario: As a user, I want to convert currency from AUD to USD
Given I launch the app
When I Enter "6.73" in the to currency field
Then I should see correct results in from currency
