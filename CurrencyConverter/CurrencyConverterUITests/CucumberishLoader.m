//
//  CucumberishLoader.m
//  CurrencyConverterUITests
//
//  Created by Ganesh Faterpekar on 6/10/20.
//

#import <Foundation/Foundation.h>

#import "CurrencyConverterUITests-Swift.h"

__attribute__((constructor))
void CucumberishInit(){
    
    [CucumberishInitializer setupCucumberish];
}
