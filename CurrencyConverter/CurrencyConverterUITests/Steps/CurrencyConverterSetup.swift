//
//  CurrencyConverterSetup.swift
//  CurrencyConverterUITests
//
//  Created by Ganesh Faterpekar on 6/10/20.
//

import Foundation
import XCTest
import Cucumberish

class CurrencyConverterSetup {
    func run() {
        check_initial_setup()
    }
    
    func check_initial_setup() {
        Given("I launch the app") { _ , _ in
          XCUIApplication().launch()
        }
    
        Then("I should see Currency Calculator") { _ , _ in
           
            let fromlabelExists = XCUIApplication().staticTexts[FromCurrencyIdentifer.title].exists
            XCTAssertTrue(fromlabelExists, "From Title is not visible")
            
            let frombtnExists = XCUIApplication().buttons[FromCurrencyIdentifer.label].exists
            XCTAssertTrue(frombtnExists, "From Currency Button not visible")
            
            let fromtextExists = XCUIApplication().textFields[FromCurrencyIdentifer.textfield].exists
            XCTAssertTrue(fromtextExists, "From Currency Button not visible")
            
            let tolabelExists = XCUIApplication().staticTexts[ToCurrencyIdentifer.title].exists
            XCTAssertTrue(tolabelExists, "From Title is not visible")
            
            let tobtnExists = XCUIApplication().buttons[ToCurrencyIdentifer.label].exists
            XCTAssertTrue(tobtnExists, "To Currency Textfield not visible")
            
            let totextExists = XCUIApplication().textFields[ToCurrencyIdentifer.textfield].exists
            XCTAssertTrue(totextExists, "To Currency Textfield not visible")
            
            let lastUpdatedExists = XCUIApplication().staticTexts[AccessibilityIdentifer.lastUpdated].exists
            XCTAssertTrue(lastUpdatedExists, "Lastupdated not visible")
        
        }
    }
}
