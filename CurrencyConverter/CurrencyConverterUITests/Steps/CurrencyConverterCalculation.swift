//
//  CurrencyConverterCalculation.swift
//  CurrencyConverterUITests
//
//  Created by Ganesh Faterpekar on 6/10/20.
//

import Foundation
import XCTest
import Cucumberish
import Combine

@testable import CurrencyConverter
class CurrencyConverterCalculation {
    func run() {
       convert_currency_International_To_AUD()
       convert_currency_AUD_To_International()
    }
    
    func convert_currency_International_To_AUD() {
        Given("I launch the app") { _ , _ in
          XCUIApplication().launch()
        }
       
        When("I Enter \"([^\\\"]*)\" in the from currency field") { args, _ in
            if let amount = args {
                let fromAmmount = XCUIApplication().textFields[FromCurrencyIdentifer.textfield]
                fromAmmount.tap()
                fromAmmount.typeText(amount[0])

            }
        }
    
        Then("I should see correct results in to currency") { args , _ in
            
            if let amount = args {
                let currencyProviderMock = MockCurrencyProvider()
                
                _ = currencyProviderMock.getCurrencies().sink { _ in
                    
                } receiveValue: { currencyresposne in
                    let ProductDetails = currencyresposne.data.Brands.WBC.Portfolios.FX.Products
                    let currencyVM = CurrencyUIViewModel(currencyDetails: ProductDetails[DefaultCurrencyCode.fromCurrency]?.Rates[DefaultCurrencyCode.fromCurrency] as! CurrencyRepresentable )
                    
                    //USD -> AUD Conversion
                    let convertedValue = currencyVM.calculateForex(type: .from, amount: amount[0])
                    let toTextExists = XCUIApplication().textFields[ToCurrencyIdentifer.textfield]
                    let value = toTextExists.value as! String
                    XCTAssertEqual(value, convertedValue.formatted)
                }
            }
           
        
        }
    }
    
    
    func convert_currency_AUD_To_International() {
        Given("I launch the app") { _ , _ in
          XCUIApplication().launch()
        }
       
        When("I Enter \"([^\\\"]*)\" in the to currency field") { args, _ in
            if let amount = args {
                let fromAmmount = XCUIApplication().textFields[ToCurrencyIdentifer.textfield]
                fromAmmount.tap()
                fromAmmount.typeText(amount[0])

            }
        }
    
        Then("I should see correct results in from currency") { args , _ in
            
        if let amount = args {
            let currencyProviderMock = MockCurrencyProvider()
            
            _ = currencyProviderMock.getCurrencies().sink { _ in
                 
             } receiveValue: { currencyresposne in
                let ProductDetails = currencyresposne.data.Brands.WBC.Portfolios.FX.Products
                let currencyVM = CurrencyUIViewModel(currencyDetails: ProductDetails[DefaultCurrencyCode.fromCurrency]?.Rates[DefaultCurrencyCode.fromCurrency] as! CurrencyRepresentable )
                
            
                //AUD -> AUD Conversion
                let  convertedValue = currencyVM.calculateForex(type: .to, amount: amount[0])
                let fromTextExists = XCUIApplication().textFields[FromCurrencyIdentifer.textfield]
                let value = fromTextExists.value as! String
                XCTAssertEqual(value, convertedValue.formatted)
             }
            
        }
        
        }
    }

}
