//
//  CurrencyHelper.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 18/10/20.
//

import Foundation
///Dictionary of flags representing the currencies
let currenciesFlags : [String:String] =  ["AED": "🇦🇪","ARS": "🇦🇷", "AUD": "🇦🇺","BDT": "🇸🇦",
                                          "BND": "🇧🇳", "BRL": "🇧🇷","CAD": "🇨🇦","CHF": "🇨🇭",
                                          "CLP": "🇨🇱","CNH": "🇨🇳","CNY": "🇨🇳","DKK": "🇩🇰",
                                          "EUR": "🇪🇺","FJD": "🇫🇯","GBP": "🇬🇧","HKD": "🇭🇰",
                                          "IDR": "🇮🇩","INR": "🇮🇳","JPY": "🇯🇵","KRW": "🇰🇷",
                                          "LKR": "🇱🇰","MYR": "🇲🇾","NOK": "🇳🇴","NZD": "🇳🇿",
                                          "PGK": "🇵🇬","PHP": "🇵🇭","PKR": "🇵🇰","SAR": "🇸🇦",
                                          "SBD": "🇸🇧","SEK": "🇸🇪","SGD": "🇸🇬","THB": "🇹🇭",
                                          "TOP": "🇹🇴","TWD": "🇹🇼","USD": "🇺🇸","VND": "🇻🇳",
                                          "VUV": "🇻🇺","WST": "🇦🇸","XPF": "🇵🇫","ZAR": "🇿🇦"]
