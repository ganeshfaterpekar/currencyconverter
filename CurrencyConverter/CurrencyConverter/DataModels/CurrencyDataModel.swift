//
//  CurrencyDataModel.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 18/10/20.
//

import Foundation

/// Struct implementing CurrencyRepresentable protocol
/// It makes views independent of underlying concrete currency data model
struct Currencydetails: Codable , CurrencyRepresentable {
    /// Code for the currency
    var currencyCode: String
    
    /// Name of the currency
    var currencyName: String
    
    /// Name of the country representing the currency
    var country: String
    
    /// Buying price of the currency
    var buyTT: String
    
    /// Selling rice of the currency
    var sellTT: String
    
    var buyTC: String
    
    /// Notes on buying
    var buyNotes: String
    
    /// Notes on selling
    var sellNotes: String
    
    /// Spot rate  date format
    var SpotRate_Date_Fmt: String
    
    /// Effective date format
    var effectiveDate_Fmt: String
    
    /// Update date format
    var updateDate_Fmt: String
    
    /// Last update timestamp
    var lastUpdated: String
    
    private enum CodingKeys: String, CodingKey {
        case currencyCode
        case currencyName
        case country
        case buyTT
        case sellTT
        case buyTC
        case buyNotes
        case sellNotes
        case SpotRate_Date_Fmt
        case effectiveDate_Fmt
        case updateDate_Fmt
        case lastUpdated = "LASTUPDATED"
    }
}
