//
//  CurrencyForexResponse.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 5/10/20.
//

import Foundation
struct CurrencyForexResponse: Codable {
    var  apiVersion: String
    var  status: Int
    var  data: FXData
}

struct FXData: Codable {
    var Brands: Brand
}

struct Brand: Codable {
    var WBC: WCB
}

struct WCB: Codable {
    var  Brand: String
    var  Portfolios: Portfolios
}

struct Portfolios: Codable {
    var FX: FX
}

struct FX: Codable {
    var PortfolioId: String
    var Products:[String:ProductDetails]
}

struct ProductDetails: Codable {
    var  ProductId: String
    var  Rates: [String:Currencydetails]
}

/// Help Struct for calculating forex rates
 struct ForexRates {
     var rates: [String:ProductDetails]
    
     init(rates : [String:ProductDetails]) {
        self.rates = rates
    }
    
    /// Returns currency details
     func getCurrencyDetailsRates(forKey key : String) -> Currencydetails? {
        if let rates = rates[key] {
            return rates.Rates[key]
        }
        
        return nil
    }
    
    /// Returns all the currency codes
    func getAllCurrencyCode() -> [String] {
        let sortedKeys = Array(rates.keys).sorted()
        return sortedKeys
    }
    
    /// Returns supported currency for transactionType
    func getSupportedCountryCode(type : TransactionType) -> [String] {

        let buyCurrencies =  rates.filter { (key,productdetails) -> Bool in
            let currencyDetails = productdetails.Rates[key]
            if type == .buy {
                guard let _ = Double(currencyDetails!.buyTT) else {
                    return false
                }
            } else {
                guard let _ = Double(currencyDetails!.sellTT) else {
                    return false
                }
                
            }
            return true
        }

        let sortedKeys = Array(buyCurrencies.keys).sorted()
        return sortedKeys
    }
    
}
