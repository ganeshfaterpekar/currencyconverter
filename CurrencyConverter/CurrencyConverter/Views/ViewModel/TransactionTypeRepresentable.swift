//
//  TransactionTypeRepresentable.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 5/10/20.
//

import Foundation

public protocol TransactionTypeRepresentable {
    
}

public enum TransactionType :Int, TransactionTypeRepresentable {
    case buy
    case sell
}
