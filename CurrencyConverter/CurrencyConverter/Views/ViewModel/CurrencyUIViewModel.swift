//
//  CurrencyUIViewModel.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 5/10/20.
//

import Foundation

/// Enum for identifying to Currency coversion flow
enum TransactionFlow {
    case from
    case to
}
struct CurrencyUIViewModel {
    var currencyDetails: CurrencyRepresentable
    
    public init(currencyDetails : CurrencyRepresentable) {
        self.currencyDetails = currencyDetails
    }
    
//    public var toUnitRates: String {
//        return "1 AUD = \(currencyValue) \(currencyDetails.currencyCode)"
//    }
    
//    public var fromCountryFlag: String {
//        return currencyDetails.currencyCode.uppercased()
//    }
    
    public var lastUpdatedLabel: String {
        return "Last updated on: \(currencyDetails.lastUpdated)"
    }
    
    public var transactionType: TransactionType = .buy
    
    public var currencyValue: Double {
        if transactionType == .buy {
            return (Double(currencyDetails.buyTT)!)
        } else {
            return (Double(currencyDetails.sellTT)!)
        }
    }
    
    public var fromUnitRateValue: Double {
        return Double(1/currencyValue)
    }
    
    public var toUnitRateValue: Double {
        return Double(currencyValue)
    }
    
    public func calculateForex(type: TransactionFlow, amount: String) -> Double {
        if type == .from {
            if let value = Double(amount) {
                return (fromUnitRateValue) * value
            }
        } else {
            if let value = Double(amount) {
                return (toUnitRateValue) * value
            }
        }
    
        return 0.0
    }

}
