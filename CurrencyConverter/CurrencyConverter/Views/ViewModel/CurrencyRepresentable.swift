//
//  CurrencyRepresentable.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 5/10/20.
//

import Foundation
/// Protocol for representing the Currency Object
///
/// Implement this protocol to make Views independent of currency data model

public protocol CurrencyRepresentable {
    /// Code for the Currency
    var currencyCode: String { get }
    
    /// Name of the Currency
    var currencyName: String { get }
    
    /// Name of the Country
    var country: String { get }
    
    /// Buying Price of the Currency
    var buyTT: String { get }
    
    /// Selling Price of the Currency
    var sellTT: String { get }
    
    /// Lastupdate timestamp
    var lastUpdated: String { get }
}
