//
//  CurrencyCalculatorView.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 6/10/20.
//

import SwiftUI

/// CurrencyCalculatorView is the component enclosing the all currency view
public struct CurrencyCalculatorView: View {
    
    @StateObject private var currencyModel = CurrencyProvider()
    @State private var selectedCountry = DefaultCurrencyCode.fromCurrency
    @State private var fromValue = ""
    @State private var toValue = ""
    @State private var isModal: Bool = false
   
    /// Selected Theme
    ///
    /// There two theme Default and SkyBlue
    var theme : Themable = Theme().themeName
    
    /// Indicates number the can be entered by the user
    let characterLimit = 10
    
    /// Initializer
    public init() { }
    
    /// View body is composed of following View
    /// 1. From Country view
    /// 2. To Country view
    /// 3. Fotter view
    public var body: some View {
        let frombinding = Binding<String> { () -> String in
            self.fromValue
            
        } set: { (value) in
            
            if (value.count < characterLimit) {
                self.fromValue = value
                var intvalue = 0.0
                
                if let v = Double(value) {
                    intvalue = v
                }
                let calcutedValue =  currencyModel.currencyViewModel!.calculateForex(type: .from, amount: String(intvalue)).formatted
                self.toValue = calcutedValue
            } else {
                self.fromValue = String(self.fromValue.dropLast())
            }
        }
        
        let tobinding = Binding<String> { () -> String in
            self.toValue
        } set: { (value) in
            self.toValue = value
            var intvalue = 0.0
            
            if let v = Double(value) {
                intvalue = v
            }
            
            let calcutedValue =  currencyModel.currencyViewModel!.calculateForex(type: .to, amount: String(intvalue)).formatted
            self.fromValue = calcutedValue
            
        }
        
        NavigationView {
            VStack {
                HStack {
                    showFromCountry(frombinding)
                }.padding(EdgeInsets(top: 10, leading: 10, bottom: 0, trailing: 10))
                
                HStack {
                    showToCountry(tobinding)
                }.padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10))
                
                HStack {
                    showFooter()
                }.padding()
                
                Spacer()
            }
            .background(Color.init(theme.containerBackgroundColor))
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    HStack {
                        Image(systemName: "dollarsign.circle")
                        Text("Currency Convertor").font(.headline)
                    }
                }
            }
            
        }
    }
    
    fileprivate func showToCountry(_ tobinding: Binding<String>) -> some View {
        return HStack {
            VStack {
                Text("I Want")
                    .accessibility(identifier: ToCurrencyIdentifer.title)
                    .foregroundColor(Color.init(theme.labelColor))
                HStack {
                    TextField("To Currency", text:  tobinding )
                        .accessibility(identifier: ToCurrencyIdentifer.textfield)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .keyboardType(.numberPad)
                    Button {
                        
                    } label: {
                        Text(DefaultCurrencyCode.toCurrency)
                            .foregroundColor(Color.init(theme.labelColor))
                        Text(currenciesFlags[DefaultCurrencyCode.toCurrency] ?? "AUD").font(.largeTitle)
                        
                    }.accessibility(identifier: ToCurrencyIdentifer.label)
                }
            }.padding()
            .background(Color.init(theme.controlBackgroundColor))
        }.padding()
    }
    
    fileprivate func showFooter() -> some View {
        return VStack {
            HStack {
                Text(currencyModel.currencyViewModel?.lastUpdatedLabel ?? "")
                    .accessibility(identifier: AccessibilityIdentifer.lastUpdated)
                    .foregroundColor(Color.init(theme.labelColor))
            }

        }
    }
    
    fileprivate func showFromCountry(_ frombinding: Binding<String>) -> some View {
        return VStack {
            VStack {
                Text("I Have")
                    .accessibility(identifier: FromCurrencyIdentifer.title)
                    .foregroundColor(Color.init(theme.labelColor))
                HStack {
                    TextField("From Currency", text: frombinding)
                        .accessibility(identifier: FromCurrencyIdentifer.textfield)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .keyboardType(.numberPad)
                      
                    Button {
                        self.isModal = true
                    }
                    label: {
                        Text(selectedCountry)
                            .foregroundColor(Color.init(theme.labelColor))
                        Text(currenciesFlags[selectedCountry] ?? "").font(.largeTitle)
                            .background(Color.init(theme.buttonBackgroundColor))
                            .clipShape(Circle())
                       
                    }.accessibility(identifier: FromCurrencyIdentifer.label)
                    .sheet(isPresented: $isModal, content: {
                        if let products = currencyModel.currencies?.data.Brands.WBC.Portfolios.FX.Products {
                            let forex = ForexRates(rates: products)
                            CurrencyCountryListView(forexDetails: forex, selectedCounty: $selectedCountry, isPresented: $isModal, fromValue: $fromValue,toValue: $toValue , vm:$currencyModel.currencyViewModel )
                        }
                    })
                }
            }.padding()
            .background(Color.init(theme.controlBackgroundColor))
        }.padding()
            
        }
}

struct CurrencyCalculatorView_Previews: PreviewProvider {
    static var previews: some View {
        CurrencyCalculatorView()            
    }
}
