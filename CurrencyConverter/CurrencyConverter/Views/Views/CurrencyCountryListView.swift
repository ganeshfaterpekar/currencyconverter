//
//  CurrencyCountryList.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 6/10/20.
//

import SwiftUI

public struct CurrencyCountryListView: View {
    var forexDetails: ForexRates
    @Binding var selectedCounty: String
    @Binding var isPresented: Bool
    @Binding var fromValue: String
    @Binding var toValue: String
    @Binding var vm: CurrencyUIViewModel?
    
    public var body: some View {
        NavigationView {
            List(forexDetails.getSupportedCountryCode(type: .buy),id: \.self) { data in
                HStack {
                   Text(currenciesFlags[data] ?? "" ).font(.largeTitle)
                   Text(data)
                }.onTapGesture {
                    self.selectedCounty = data
                self.vm = CurrencyUIViewModel(currencyDetails: forexDetails.getCurrencyDetailsRates(forKey: data) as! CurrencyRepresentable)
                self.toValue =  String((self.vm?.calculateForex(type: .from, amount: self.fromValue))!)
                self.isPresented = false
                
            }
            }
            .navigationBarTitle("Select Currency")
        }
    }
}
