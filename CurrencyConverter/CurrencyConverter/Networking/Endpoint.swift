//
//  Endpoint.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 4/10/20.
//

import Foundation

/// Endpoint struct for the request
///
///
struct Endpoint {
    var path:  String
    var queryItems: [URLQueryItem] = []
}

extension Endpoint {
    var url: URL {
       var components = URLComponents()
        components.scheme = "https"
        components.host = "www.westpac.com.au"
        components.path = "/bin" + path
        components.queryItems = queryItems
        guard let url = components.url else {
            preconditionFailure("Invalid url")
        }
        return url
    }
    
    var headers: [String:Any] {
        return [
            "Content-Type":"application/json"
        ]
    }
}
