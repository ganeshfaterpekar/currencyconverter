//
//  APIClient.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 4/10/20.
//

import Foundation
import Combine

typealias DataTaskResult = URLSession.DataTaskPublisher.Output

protocol URLSessionProtocol {
    func apiRespose(for request: URLRequest) -> AnyPublisher<DataTaskResult,URLError>
}

extension URLSession: URLSessionProtocol {
    func apiRespose(for request: URLRequest) -> AnyPublisher<DataTaskResult, URLError> {
        return dataTaskPublisher(for: request).eraseToAnyPublisher()
    }
}

protocol APIClient {
    var session: URLSessionProtocol { get }
    func runRequest<T> (_ request : URLRequest , type: T.Type , queue: DispatchQueue) -> AnyPublisher <T, Error> where T: Codable
}

extension APIClient {
    func runRequest<T: Codable>(_ request : URLRequest , type: T.Type , queue: DispatchQueue) -> AnyPublisher <T, Error> {
        return session.apiRespose(for: request)
            .tryMap {
                guard let httpResponse = $0.response as? HTTPURLResponse else {
                    throw APIError.unknow
                }

                if (httpResponse.statusCode == 401) {
                    throw APIError.apiError(reason: "Unauthorized")
                }
                if (httpResponse.statusCode == 403) {
                    throw APIError.apiError(reason: "Resource forbidden")
                }
                if (httpResponse.statusCode == 404) {
                    throw APIError.apiError(reason: "Resource not found")
                }
                if (405..<500 ~= httpResponse.statusCode) {
                    throw APIError.apiError(reason: "client error")
                }
                if (500..<600 ~= httpResponse.statusCode) {
                    throw APIError.apiError(reason: "server error")
                }
                return $0.data
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .receive(on: queue)
            .eraseToAnyPublisher()
    
    }

}
