//
//  APIError.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 4/10/20.
//

import Foundation

/// Returns an APIError
///
/// Use this emum when you want to return APIError. Add New cases to extend the functionality
/// - unknow: All the unknown Errros
/// - apiError: Error specific to the API
/// - parsingError: Error resulted while decoding the json
/// - httpError: Error due to http failures
enum APIError: Error {
    case unknow
    case apiError(reason: String)
    case parsingError(reason: String)
    case httpError(statusCode : URLError)
    
    var localizedDescription: String {
        switch self {
        case .unknow: return "Unknown error"
        case .apiError(let reason) : return reason
        case .parsingError(let reason): return reason
        case .httpError(let from): return  from.localizedDescription
        }
    }
}
