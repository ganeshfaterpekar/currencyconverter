//
//  Defaults.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 5/10/20.
//

import Foundation

/// Enum for the default currency code. This currencies will be shown on load
enum DefaultCurrencyCode {
    ///Set the default from currency here. Currently set USD
    static let fromCurrency = "USD"
    ///Set the default to currency here. Currently set AUD
    static let toCurrency = "AUD"
}

/// Enum for the AccessibilityIdentifer
enum FromCurrencyIdentifer {
    /// Accessibility Identifer for the "from country" label
    static let label = "fromcountry_label"
    
    /// Accessibility Identifer for the "from country" textfield
    static let textfield = "fromcountry_textfield"
    
    /// Accessibility Identifer for the "from country" header label
    static let title = "fromtitle"
}

/// Enum for the AccessibilityIdentifer
enum ToCurrencyIdentifer {
    /// Accessibility Identifer for the "from country" label
    static let label = "tocountry_label"
    
    /// Accessibility Identifer for the "to country" textfield
    static let textfield = "tocountry_textfield"
    
    /// Accessibility Identifer for the "to country" header label
    static let title = "totitle"
}

/// Enum for the AccessibilityIdentifer
enum AccessibilityIdentifer {
    /// Accessibility Identifer for the lastupdated timestamp label
    static let lastUpdated = "lastUpdated"

}
