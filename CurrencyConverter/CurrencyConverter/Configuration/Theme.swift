//
//  Theme.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 5/10/20.
//

import UIKit
import SwiftUI

extension UIFont {
    static let defaultLightFont: UIFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)!
    static let defaultRegularFont: UIFont = UIFont(name: "HelveticaNeue-Medium", size: 17.0)!
    static let defaultBoldFont: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 18.0)!
    
    static let skyLightFont: UIFont = UIFont(name: "Avenir-Light", size: 12.0)!
    static let skyRegularFont: UIFont = UIFont(name: "Avenir-Medium", size: 17.0)!
    static let skyBoldFont: UIFont = UIFont(name: "Avenir-Heavy", size: 18.0)!
}

public protocol Themable {
    var lightLabelFont: UIFont { get }
    var regularLabelFont: UIFont { get }
    var boldLabelFont: UIFont { get }
    var labelColor: UIColor { get }
    var textFieldColor: UIColor { get}
    var controlBackgroundColor: UIColor { get}
    var containerBackgroundColor: UIColor { get}
    var buttonBackgroundColor: UIColor { get}
}

struct Theme {
    
    var themeName : Themable {
         {
            var theme = "Basic"
            if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
                if let dic = NSDictionary(contentsOfFile: path) {
                    theme =  dic["THEME"] as? String ?? theme
                }
            }
            
            if (theme == "Blue") {
                return SkyTheme()
            }
            
            return DefaultTheme()
         }()
    }
    
    func getTheme(_ isDarkMode: Bool) -> Themable {
        if (isDarkMode) {
            return SkyTheme()
        }
        
        return DefaultTheme()
    }

}

public struct DefaultTheme: Themable {
    public let lightLabelFont: UIFont = .defaultLightFont
    public let regularLabelFont: UIFont = .defaultRegularFont
    public let boldLabelFont: UIFont = .defaultBoldFont
    public let labelColor: UIColor = .defaultTextColor
    public let textFieldColor: UIColor = .defaultTextColor
    public let controlBackgroundColor: UIColor = .defaultControlBgColor
    public let containerBackgroundColor: UIColor = .defaultContainerBgColor
    public let buttonBackgroundColor: UIColor = .defaultButtonBgColor
    
    public init() {}
}

public struct SkyTheme: Themable {
    public let lightLabelFont: UIFont = .skyLightFont
    public let regularLabelFont: UIFont = .skyRegularFont
    public let boldLabelFont: UIFont = .skyBoldFont
    public let labelColor: UIColor = .skyTextColor
    public let textFieldColor: UIColor = .skyControlBgColor
    public let controlBackgroundColor: UIColor = .skyControlBgColor
    public let containerBackgroundColor: UIColor = .skyContainerBgColor
    public let buttonBackgroundColor: UIColor = .skyButtonBgColor
    
    public init() {}
}
