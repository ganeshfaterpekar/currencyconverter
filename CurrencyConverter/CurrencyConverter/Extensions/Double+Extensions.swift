//
//  Double+Extensions.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 18/10/20.
//

import Foundation

/// Extension on Dobule data type
extension Double {
    /// Returns Double upto two decimal places
    var formatted : String {
        return String(format: "%.02f", self)
    }
}
