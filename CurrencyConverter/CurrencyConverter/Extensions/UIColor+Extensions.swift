//
//  UIColor+Extensions.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 18/10/20.
//

import UIKit

/// Creates an UIColor from Hex String
extension UIColor {
    
/// Creates an UIColor from HEX String in "#FFA69E" format
///  - parameter hexString: HEX String in "#FFA69E" format
///  - returns: UIColor from HexString

    convenience init(hexString: String) {
        
        let hexString: String = (hexString as NSString).trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner          = Scanner(string: hexString as String)
        
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:1)
    }

/// Creates an UIColor Object based on provided RGB value in integer
/// - parameter red:   Red Value in integer (0-255)
/// - parameter green: Green Value in integer (0-255)
/// - parameter blue:  Blue Value in integer (0-255)
/// - returns: UIColor with specified RGB values
     
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
}

/// Extension defines the Color scheme used for Theme
extension UIColor {
    
    /// Default Color for all the text. The default textcolor is black
    /// Used in Default Theme
    static let defaultTextColor = UIColor(hexString: "#621a4b")
    
    /// Default background color of currency calculator compontent ie From / To Currencies
    /// Used in Default Theme
    static let defaultControlBgColor = UIColor(hexString:"#d7d2cb")
    
    /// Default background color of the container enclosing currency calculator compontent
    /// Used in Default Theme
    static let defaultContainerBgColor = UIColor(hexString:"#f4f3f0")
    
    /// Default Color currency change button.
    /// Used in Default Blue Theme
    static let defaultButtonBgColor =  UIColor.gray
    
    /// Default Color for all the text. The default textcolor is white
    /// Used in Blue Theme
    static let skyTextColor = UIColor.white
    
    /// Default Color for the text. The default textcolor is Blue
    /// Used in Blue Theme
    static let skyControlBgColor = UIColor(hexString:"#124B6B")
    
    /// Default Color for the text. The default textcolor is Blue
    /// Used in Blue Theme
    static let skyContainerBgColor = UIColor(hexString:"#94B4C6")
    
    /// Default Color currency change button.
    /// Used in Blue Theme
    static let skyButtonBgColor =  UIColor(hexString:"#00b4d8")
    
}
