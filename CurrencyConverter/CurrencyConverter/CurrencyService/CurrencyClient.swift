//
//  CurrencyClient.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 5/10/20.
//

import Foundation
import Combine

/// Fetches the currencies from the APIService
///
/// Add functions here  to fetch any other data from the APIService

class CurrencyClient : APIClient {
    var session: URLSessionProtocol
    
    ///Initializer
    init(configuration : URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    ///Initializer
    convenience init() {
        self.init(configuration: .default)
    }
    
    ///Get the forex rates from the API service
    func getForexRates(endpoint: Endpoint) -> AnyPublisher<CurrencyForexResponse,Error> {
        let urlRequest = URLRequest(url: endpoint.url)
        return runRequest(urlRequest, type: CurrencyForexResponse.self, queue: DispatchQueue.main)
    }
    
}
