//
//  CurrencyFeed.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 5/10/20.
//

import Foundation

/// List of Endpoint used for Currency Service
///
/// Addition Endpoint can be added as follow
///  static var supportedCurrencies: Self {
///      return Endpoint(path: "/getSupportedCurrencies")
///  }
///
extension Endpoint {
    static var forex: Self {
        return Endpoint(path: "/getJsonRates.wbc.fx.json")
    }
    
}
