//
//  CurrencyProvider.swift
//  CurrencyConverter
//
//  Created by Ganesh Faterpekar on 5/10/20.
//

import Foundation
import Combine

/// Provides Currency data to Views
///
///
class CurrencyProvider: ObservableObject {
    
    ///Use to store to API response
    @Published var currencies: CurrencyForexResponse?
    
    ///View model for the Currency view
    @Published var currencyViewModel: CurrencyUIViewModel?
    
    private var cancellable: AnyCancellable?
    
    private let client =  CurrencyClient()
    
    init() {
        cancellable = client.getForexRates(endpoint: Endpoint.forex)
            .sink(receiveCompletion: { _ in
                
            }, receiveValue: { currencyresposne in
               
                let ProductDetails = currencyresposne.data.Brands.WBC.Portfolios.FX.Products
                let currencyVM = CurrencyUIViewModel(currencyDetails: ProductDetails[DefaultCurrencyCode.fromCurrency]?.Rates[DefaultCurrencyCode.fromCurrency] as! CurrencyRepresentable )
                self.currencyViewModel = currencyVM
                self.currencies = currencyresposne
            })
    }
    
}
