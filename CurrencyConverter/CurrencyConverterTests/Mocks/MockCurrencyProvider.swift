//
//  MockCurrencyProvider.swift
//  CurrencyConverterTests
//
//  Created by Ganesh Faterpekar on 18/10/20.
//

import Foundation
import Combine

class MockCurrencyProvider {
    func getCurrencies() -> AnyPublisher<CurrencyForexResponse,Error> {
        let currencyClient = MockAPIClient(session: MockURLSession())
        return currencyClient.getData()
    }

}
