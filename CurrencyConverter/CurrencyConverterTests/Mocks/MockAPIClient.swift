//
//  MockAPIClient.swift
//  CurrencyConverterTests
//
//  Created by Ganesh Faterpekar on 18/10/20.
//

import Foundation
import Combine

struct MockAPIClient: APIClient {
    var session: URLSessionProtocol
    func getData() -> AnyPublisher<CurrencyForexResponse,Error> {
        let urlRequest = URLRequest(url:  Endpoint.forex.url)
        return runRequest(urlRequest, type: CurrencyForexResponse.self, queue: DispatchQueue.main)
        
    }
}
