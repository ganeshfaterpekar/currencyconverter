//
//  MockURLSession.swift
//  CurrencyConverterTests
//
//  Created by Ganesh Faterpekar on 17/10/20.
//

import Foundation
import Combine
@testable import CurrencyConverter

class MockURLSession: URLSessionProtocol {
    func apiRespose(for request: URLRequest) -> AnyPublisher<DataTaskResult, URLError> {
        let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: "HTTP/1.1", headerFields: nil)!
        let data = try! StubHelper().readJsonfile()
        return Just((data: data, response: response))
            .setFailureType(to: URLError.self)
            .eraseToAnyPublisher()
    }
    
}
