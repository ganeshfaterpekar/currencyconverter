//
//  CurrencyClientTest.swift
//  CurrencyConverterTests
//
//  Created by Ganesh Faterpekar on 17/10/20.
//

import XCTest
import Combine
@testable import CurrencyConverter

class CurrencyClientTest: XCTestCase {

    var cancellabel : AnyCancellable?
    var expectation = XCTestExpectation(description: "")
    let currencyProviderMock = MockCurrencyProvider()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        expectation = XCTestExpectation(description: "Download the mock data")
        cancellabel = currencyProviderMock.getCurrencies().sink { _ in
            
        } receiveValue: { currencyresposne in
            XCTAssertNotNil(currencyresposne)
            self.expectation.fulfill()
        }
        
        wait(for: [self.expectation], timeout: 5.0)
    }
    
    func testConversion(){
        expectation = XCTestExpectation(description: "Test Currency Coversion")
        cancellabel = currencyProviderMock.getCurrencies().sink { _ in
             
         } receiveValue: { currencyresposne in
            let ProductDetails = currencyresposne.data.Brands.WBC.Portfolios.FX.Products
            let currencyVM = CurrencyUIViewModel(currencyDetails: ProductDetails[DefaultCurrencyCode.fromCurrency]?.Rates[DefaultCurrencyCode.fromCurrency] as! CurrencyRepresentable )
            
            //USD -> AUD Conversion
            var convertedValue = currencyVM.calculateForex(type: .from, amount: "5")
            XCTAssertEqual(convertedValue.formatted, "6.73")
            
            //AUD -> AUD Conversion
            convertedValue = currencyVM.calculateForex(type: .to, amount: "6.73")
            XCTAssertEqual(convertedValue.formatted, "5.00")
            
            
             self.expectation.fulfill()
         }
        
        wait(for: [self.expectation], timeout: 5.0)
    }

}
