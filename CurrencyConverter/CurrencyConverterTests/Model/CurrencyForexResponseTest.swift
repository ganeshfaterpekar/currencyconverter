//
//  CurrencyForexResponseTest.swift
//  CurrencyConverterTests
//
//  Created by Ganesh Faterpekar on 6/10/20.
//

import XCTest
import Foundation
@testable import CurrencyConverter

class CurrencyForexResponseTest: XCTestCase {
    
    var data: Data?
    var sut: CurrencyForexResponse?
    var rates : [String:ProductDetails]?
    var forexRates : ForexRates?

    override func setUpWithError() throws {

        data = loadStubFromBundle(name: "forex", ext: "json")
        sut = try!JSONDecoder().decode(CurrencyForexResponse.self, from: data!)
        rates = sut?.data.Brands.WBC.Portfolios.FX.Products
        forexRates = ForexRates(rates: rates!)
    }

    func testGetAllCurrencyCode_Returns_SortedArrayOfCurrencyCode() {
        let currencyCode = forexRates?.getAllCurrencyCode()
        XCTAssertEqual(currencyCode![0], "AED")
    }
    
}
