//
//  CurrencyUIViewModelTest.swift
//  CurrencyConverterTests
//
//  Created by Ganesh Faterpekar on 18/10/20.
//

import XCTest
import Foundation

class CurrencyUIViewModelTest: XCTestCase {
    
    var currencyDetails: CurrencyRepresentable?
    var currencyVM: CurrencyUIViewModel?
    
    override func setUpWithError() throws {
       
        currencyDetails = Currencydetails(currencyCode: "USD",
                                         currencyName: "Dollars",
                                         country: "USA",
                                         buyTT: "0.7430",
                                         sellTT: "0.6716",
                                         buyTC: "0.7455",
                                         buyNotes: "N/A",
                                         sellNotes: "N/A",
                                         SpotRate_Date_Fmt: "20201016",
                                         effectiveDate_Fmt: "20201016T074200,66+11:00",
                                         updateDate_Fmt: "20201016T074200,66+11:00",
                                         lastUpdated: "07:10 AM 16 Oct 2020")
        
        currencyVM = CurrencyUIViewModel(currencyDetails: currencyDetails!)
    }
    
    func test_SetLastupdateTimeStamp() {
        XCTAssertEqual( currencyVM?.lastUpdatedLabel ,"Last updated on: 07:10 AM 16 Oct 2020")
    }
    
    func test_Amount_When_From_Currency_isEntered() {
        //USD -> AUD Conversion
        if let val = currencyVM?.calculateForex(type: .from, amount: "5") {
            XCTAssertEqual(val.formatted, "6.73")
        }
    }
    
    func test_Amount_When_To_Currency_isEntered() {
        //AUD -> AUD Conversion
        if let val = currencyVM?.calculateForex(type: .to, amount: "6.73") {
            XCTAssertEqual(val.formatted, "5.00")
        }
    }
    
}
