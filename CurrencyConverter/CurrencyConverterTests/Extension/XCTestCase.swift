//
//  XCTestCase.swift
//  CurrencyConverterTests
//
//  Created by Ganesh Faterpekar on 6/10/20.
//

import XCTest

extension XCTestCase {
    
    func loadStubFromBundle(name: String, ext: String) -> Data {
        let bundle = Bundle(for: classForCoder)
        let url = bundle.url(forResource: name, withExtension: ext)
        
        return try! Data(contentsOf: url!)
    }
}
