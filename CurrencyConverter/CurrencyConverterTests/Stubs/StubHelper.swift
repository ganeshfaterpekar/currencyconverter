//
//  StubHelper.swift
//  CurrencyConverterTests
//
//  Created by Ganesh Faterpekar on 18/10/20.
//

import Foundation


class StubHelper {
    
    func readJsonfile() throws -> Data {
        let bundle = Bundle(for: type (of: self))
        guard let url = bundle.url(forResource: "forex", withExtension: "json") else {
            return Data("{No Valid data}".utf8)
        }
        return try Data(contentsOf: url)
        
    }
    
}
