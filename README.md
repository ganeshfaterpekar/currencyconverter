#Mobile Application Assessment

As part of your application we invite you to complete the following coding assessment.

##Overview

The task we would like you to complete is to create an app that makes a call for international currency data (JSON) and use it to create a currency converter app that converts international currencies into Australian Dollars.  Refer to Apple's Human Interface Guidelines or Android's material design as a resource to provide a good customer experience.

## Requirements

* Xcode 12.x
* iOS 14.0
* Cocoapods

## Setup

```
git clone https://gitlab.com/ganeshfaterpekar/currencyconverter.git
cd currencyconverter/Currencyconverter
pod install
```

### Architecture
## DevOps
  For every commit CI/CD is run involing the following stages
  1. Build
  2. Lint - Using Swiftlinter 
  3. Unit Testing and BDD
## Code 
Application is develop using Swift, SwiftUI and Combine framework . Latest framework has been using since I belive its the way forward.
The code is divided into different group / layers (Self explanatory)         

## Third Party
Cucumberish is used for BDD testing


## Library
The library can be download using Cocoapods
```
pod 'WPCurrencyConverterNew'
```

## Documentation
Code has been commented and Document generation is done via jazzy
To view the documentation go to 
```
cd currencyconverter/docs/index.html 
```

## Requirement Traceability


| Requirement                        |   Implemented        | Comments                                           |
| -------------                      |    :-------------:   | :-----                                             |
| Swift langauge                     | :white_check_mark:    | Done using Swift + SwiftUI + Combine frameworks    |
| Xcode 9+                           | :white_check_mark:    | Lastest Xcode used                                 |
| Desgin Patterns                    | :white_check_mark:    | Solid Principles,MVVM followed                                   |
| 3rd Party library                  | :white_check_mark:    | Create library and distributed using cocoapods    |
| Code Comments + API Generation     | :white_check_mark:    | Code commented and Document generation via jazzy  |   
| UI Interaction                     | :white_check_mark:    | Two targets based on themes                       |
| Threading                          | :white_check_mark:    | Asynchronous data download and UI updated on main thread|
| Git Commits                        | :white_check_mark:    | More than 50+ commits across 3 branches           |
| Testing                            | :white_check_mark:    | Unit testing and BDD testing                      |